import { LinkType, Locator, Package, Project } from "@yarnpkg/core";
import { NPM_PROTOCOL, STRICT_PROTOCOL } from "./constant";

async function afterAllInstalled(project: Project, locatorMap: any) {
  let aptResolve = 0;
  let npmResolve = 0;
  let totalResolve = 0;

  const storedPackages = project.storedPackages.values();
  
  for (const pkg of storedPackages) {
    if (pkg.linkType === LinkType.HARD && pkg.reference.startsWith(STRICT_PROTOCOL)) {
      aptResolve++;
      totalResolve++
    } else if (pkg.linkType === LinkType.SOFT && pkg.reference.startsWith(NPM_PROTOCOL)) {
      npmResolve++;
      totalResolve++
    }
  }

  console.log(`Total ${totalResolve} node modules installed (${aptResolve} via apt and ${npmResolve} via npm).`)
}

export { afterAllInstalled }