import { BaseCommand } from '@yarnpkg/cli';
import { Option } from 'clipanion';
import { Configuration, execUtils } from '@yarnpkg/core';
import { NPM_PROTOCOL, RELAXED_PROTOCOL, STRICT_PROTOCOL } from '../common/constant';

export default class AptInstallCommand extends BaseCommand {
  static paths = [
    [`apt-install`],
  ]

  defaultProtocol = RELAXED_PROTOCOL

  //Args to install production dependencies only
  commandArgs = ["workspaces", "focus", "--production"]

  local = Option.Boolean(`-l,--local`, false, {
    description: `Resolve dependencies from apt local only`,
  })

  includeDev = Option.Boolean(`-d,--includeDev`, false, {
    description: `Include devDependecies while resolving dependencies from apt`,
  })


  //   static usage: Usage = Command.Usage({
  //     description: ``,
  //     details: ``,
  //     examples: [[]],
  //   });

  // relaxed = Option.Boolean(`-R,--relaxed`, false, {
  //   description: `Resolve dependencies from apt in strict mode`,
  // })

  async execute() {
    //I don't know why this.local returns object instead of default false value when the local option is not present
    if (typeof this.local == "boolean" && this.local == true) {
      this.defaultProtocol = STRICT_PROTOCOL
    }

    if (typeof this.includeDev == "boolean" && this.includeDev == true) {
      //By default yarn adds all dependencies available in package.json
      this.commandArgs = ["install"]
    }

    await Configuration.updateConfiguration(this.context.cwd, (current: { [key: string]: unknown }) => {
      return {
        ...current,
        defaultProtocol: this.defaultProtocol
      }
    })

    console.log(`Default protocol is now ${this.defaultProtocol}`)

    let { code } = await execUtils.pipevp("yarnpkg", this.commandArgs, {
      cwd: this.context.cwd,
      stdin: this.context.stdin,
      stdout: this.context.stdout,
      stderr: this.context.stderr,
    })


    await Configuration.updateConfiguration(this.context.cwd, (current: { [key: string]: unknown }) => {
      return {
        ...current,
        defaultProtocol: NPM_PROTOCOL
      }
    })

    console.log(`Default protocol is now ${NPM_PROTOCOL}`)

    return code
  }
}